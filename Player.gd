extends KinematicBody2D

export (PackedScene) var bullet_scene

signal health_changed(Type)

const wheel_accel = 0.05
const max_wheel_speed = 0.25
const max_speed = 2000
const accel = 80
const friction = 100
const jump = -2000
const gravity = 75
const UP = Vector2(0, -1)

var motion = Vector2()
var rotation_val = 0

var health = 100

var shoot_ready = true
var invincible = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	
func _process(delta):
	if shoot_ready:
		if Input.is_action_pressed('shoot'):
			shoot_ready = false
			$ShootCooldown.start()
			
			var bullet = bullet_scene.instance()
			get_parent().add_child(bullet)
			get_parent().move_child(bullet, 3) # move bullet behind player
			get_parent().move_child(bullet, 0)
			bullet.add_to_group('bullets')
			bullet.position = $Pivot.global_position
			bullet.set_motion(motion)
			
func _physics_process(delta):
	motion = move_and_slide(motion, UP)
	
	motion.y = min(motion.y + gravity, max_speed)
	$Body.rotate(rotation_val)
	
	if is_on_floor():
		if motion.x > 0:
			rotation_val = (motion.length() / max_speed) * max_wheel_speed
		elif motion.x < 0:
			rotation_val = (-motion.length() / max_speed) * max_wheel_speed
		else:
			rotation_val = 0
			
		if Input.is_action_pressed('jump'):
			motion.y = jump
		if Input.is_action_pressed('left') and not Input.is_action_pressed('right'):
			motion.x -= accel
			motion.x = clamp(motion.x, -max_speed, 0)
		elif Input.is_action_pressed('right') and not Input.is_action_pressed('left'):
			motion.x += accel
			motion.x = clamp(motion.x, 0, max_speed)
		else:
			if motion.x > 0:
				motion.x = max(motion.x - friction, 0)
			elif motion.x < 0:
				motion.x = min(motion.x + friction, 0)
	else:
		if Input.is_action_pressed('left') and not Input.is_action_pressed('right'):
			rotation_val = max(-max_wheel_speed, rotation_val - wheel_accel)
		elif Input.is_action_pressed('right') and not Input.is_action_pressed('left'):
			rotation_val = min(max_wheel_speed, rotation_val + wheel_accel)
	

func take_damage(damage, enemy_x_pos):
	if not invincible:
		health = max(health - damage, 0)
		emit_signal("health_changed", health)
		$InvincibleTimer.start()
		invincible = true
		
		print('player: ', position.x)
		print('enemy: ', enemy_x_pos)
		
		if position.x > enemy_x_pos:
			motion = Vector2(1000,-1200)
		else:
			motion = Vector2(-1000,-1200)

func receive_health(heal):
	health = min(100, health + heal)
	emit_signal("health_changed", health)

func _on_ShootCooldown_timeout():
	shoot_ready = true
	
func set_stomped(state):
	motion.y = jump

func _on_InvincibleTimer_timeout():
	invincible = false
