extends KinematicBody2D

var motion = Vector2(0, 100)

func _ready():
	$Timer.start()

func _on_Timer_timeout():
	queue_free()
	
func _physics_process(delta):
	move_and_slide(motion)
