extends Node2D

export var max_enemies = 20
export (PackedScene) var enemy_scene

# Called when the node enters the scene tree for the first time.
func _ready():
	$Player/Camera2D.limit_left = $LeftEdge.position.x
	$Player/Camera2D.limit_right = $RightEdge.position.x
	$EnemySpawnTimer.start()

func _on_EnemySpawnTimer_timeout():
	var enemy_spawn_offset = 0
	var enemy = enemy_scene.instance()
	enemy.egg_speed = rand_range(-20, 20)
	if enemy.egg_speed > 0:
		enemy_spawn_offset = rand_range(-6000, 0)
	elif enemy.egg_speed < 0:
		enemy_spawn_offset = rand_range(0, 6000)
	enemy.position = Vector2($Player.position.x + enemy_spawn_offset, $Player.position.y - 3000)
	enemy.position.x = clamp(enemy.position.x, $LeftEdge.position.x + 300, $RightEdge.position.x - 300)
	add_child(enemy)
	enemy.add_to_group('enemy')
