extends Camera2D

const zoom_speed = 0.1

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_pressed("zoom_in"):
		zoom.x = clamp(zoom.x - zoom_speed, 2, 100)
		zoom.y = clamp(zoom.y - zoom_speed, 2, 100)
	elif Input.is_action_pressed("zoom_out"):
		zoom.x = clamp(zoom.x + zoom_speed, 2, 100)
		zoom.y = clamp(zoom.y + zoom_speed, 2, 100)
