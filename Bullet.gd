extends Area2D

const speed = 4000

var motion = Vector2()

func set_motion(in_motion):
	var direction_to_mouse = position.direction_to(get_global_mouse_position())
	position += direction_to_mouse * 10 # move bullet to the end of the cannon
	motion = direction_to_mouse * speed
	motion += in_motion

func _physics_process(delta):
	position += motion * delta

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
	
func _on_Bullet_body_entered(body):
	if body.is_in_group('enemy'):
		body.killed()
		queue_free()
