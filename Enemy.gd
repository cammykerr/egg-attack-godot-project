extends KinematicBody2D

onready var player = get_node('../Player')
export (PackedScene) var heart_scene

const UP = Vector2(0, -1)
const accel = 60

var speed = 600
var max_fall_speed = 1000
var egg_speed = 0
var gravity = 20
var is_egg = true

var motion = Vector2()

func _ready():
	$GroundForm.hide()

func _physics_process(delta):
	motion.y = min(motion.y + gravity, max_fall_speed) 
	
	if is_egg:
		if is_on_floor():
			is_egg = false
			gravity = 75
			motion.x = 0
			$GroundForm.show()
			$EggForm.hide()
		else:
			motion.x += egg_speed
			motion = motion.normalized() * min(motion.length(), max_fall_speed)
	else:
		if player.get_global_position().x > global_position.x:
			motion.x = min(speed, motion.x + accel)
		else:
			motion.x = max(-speed, motion.x - accel)

		
	motion = move_and_slide(motion, UP)

func killed():
	if is_egg:
		if randi() % 2 == 0:
			var heart = heart_scene.instance()
			heart.position = position
			get_parent().add_child(heart)
			heart.add_to_group('hearts')
	queue_free()

func _on_StompDetector_body_entered(body):
	if body.name == 'Player' and not body.invincible:
		killed()
		body.set_stomped(true)
		
