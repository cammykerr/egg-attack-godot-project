extends Area2D

func _on_HeartCollider_body_entered(body):
	if body.name == 'Player':
		body.receive_health(20)
		get_parent().queue_free()
